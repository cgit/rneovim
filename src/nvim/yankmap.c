#include "nvim/yankmap.h"

#include "nvim/memory.h"

void init_yankmap(yankmap_T* map)
{
  memset(map, 0, sizeof(yankmap_T));

  map->reg_to_yankreg = (Map(int, ptr_t))MAP_INIT;
  map->yankreg_to_reg = (Map(ptr_t, int))MAP_INIT;
  // map_init(int, ptr_t, &map->reg_to_yankreg);
  // map_init(ptr_t, int, &map->yankreg_to_reg);
}

yankreg_T* yankmap_get(yankmap_T* yankmap, int reg)
{
  bool is_new = false;
  yankreg_T** ret
    = (yankreg_T**)map_put_ref(int, ptr_t)(&yankmap->reg_to_yankreg, reg, NULL, &is_new);

  if (ret) {
    if (is_new) {
      *ret = xcalloc(1, sizeof(yankreg_T));
    }

    /* Add the back-reference */
    int* ref = map_put_ref(ptr_t, int)(&yankmap->yankreg_to_reg, *ret, NULL, NULL);
    *ref = reg;

    return *ret;
  }

  return NULL;
}

int yankmap_find(yankmap_T* yankmap, yankreg_T* yankreg)
{
  int* ref = map_ref(ptr_t, int)(&yankmap->yankreg_to_reg, yankreg, NULL);

  if (ref) {
    return *ref;
  }

  return -1;
}
